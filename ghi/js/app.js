function createCard(name, description, pictureUrl, start, end, center) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p>${center}</p>
          <p class="card-text">${description}</p>
          <p>${start} - ${end}</p>
        </div>
        
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
        const response = await fetch(url);
    
        if (!response.ok) {
          // Figure out what to do when the response is bad
          throw new Error('Response not ok');
        } else {
            const data = await response.json();
      
            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const center = details.conference.location.name
                const from = new Date(Date.parse(details.conference.starts));
                const till = new Date(Date.parse(details.conference.ends));
                const start = from.toLocaleDateString('en-US', { day: 'numeric', month: 'short', year: 'numeric' });
                const end = till.toLocaleDateString('en-US', { day: 'numeric', month: 'short', year: 'numeric' });
                const html = createCard(title, description, pictureUrl, start, end, center);
                const column = document.querySelector('.col');
                column.innerHTML += html;
              }
            }
      
          }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error('error', e);
    }
  
  });
