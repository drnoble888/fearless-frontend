window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }
    conference.classList.remove("d-none")
    const loadingWheel = document.getElementById("loading-conference-spinner")
    loadingWheel.classList.add("d-none")
  });

  window.addEventListener('DOMContentLoaded', async () => {
    
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const confHref = JSON.parse(json)
      console.log(confHref)
      //const locationUrl = `http://localhost:8001${confHref.conference}attendees/`;
      const conferenceUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
        
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newAttendee = await response.json();
        const success = document.getElementById("success-message")
        success.classList.remove("d-none")
        formTag.classList.add("d-none")
      }

      //When the response from the fetch is good, you need to remove the d-none from 
      //the success alert and add d-none to the form.
      
    });
    
  });
