window.addEventListener('DOMContentLoaded', async () => {

    let url = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(url);
        const data = await response.json()

        // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('state')
        // For each state in the states property of the data
        for(let state of data.states) {
            // Create an 'option' element
            const option = document.createElement('option')
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = state.state_abbreviation
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = state.state_name
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
        if (!response.ok) {
            throw new Error('Response not ok dummy');
          }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error('error', e);
    }
  
  });

  window.addEventListener('DOMContentLoaded', async () => {

    // State fetching code, here...
  
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
    

    });
  });
